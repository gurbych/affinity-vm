from dgllife.utils import smiles_to_bigraph, smiles_to_complete_graph, CanonicalAtomFeaturizer, CanonicalBondFeaturizer, WeaveAtomFeaturizer, WeaveEdgeFeaturizer
from functools import partial

v1 = {
    'random_seed': 8,
    'mlp_hidden_layer': 128,
    'gat_hidden_feats': [32, 32],
    'fasta_node_feat_size': 15,
    'fasta_edge_feat_size': 1,
    'smiles_num_layers': 2,
    'smiles_num_timesteps': 2,
    'smiles_graph_feat_size': 300,
    'fasta_num_layers': 2,
    'fasta_num_timesteps': 2,
    'fasta_graph_feat_size': 600,
    'n_tasks': 1,
    'dropout': 0,
    'weight_decay': 10 ** (-5.0),
    'lr': 0.001,
    'batch_size': 128,
    'num_epochs': 1000,
    'frac_train': 0.8,
    'frac_val': 0.1,
    'frac_test': 0.1,
    'patience': 100,
    'metric_name': 'mae',
    'model': 'mhGANN',
    'mode': 'lower',
    'smiles_to_graph': smiles_to_bigraph,
    'smiles_node_featurizer': CanonicalAtomFeaturizer(),
    'smiles_edge_featurizer': CanonicalBondFeaturizer(),
    'load_checkpoint': False
}

v2 = {
    'random_seed': 8,
    'mlp_hidden_layer': 256,
    'gat_hidden_feats': [64, 64],
    'fasta_node_feat_size': 15,
    'fasta_edge_feat_size': 1,
    'smiles_num_layers': 2,
    'smiles_num_timesteps': 2,
    'smiles_graph_feat_size': 300,
    'fasta_num_layers': 2,
    'fasta_num_timesteps': 2,
    'fasta_graph_feat_size': 600,
    'n_tasks': 1,
    'dropout': 0,
    'weight_decay': 10 ** (-5.0),
    'lr': 0.001,
    'batch_size': 128,
    'num_epochs': 1000,
    'frac_train': 0.8,
    'frac_val': 0.1,
    'frac_test': 0.1,
    'patience': 100,
    'metric_name': 'mae',
    'model': 'mhGANN',
    'mode': 'lower',
    'smiles_to_graph': smiles_to_bigraph,
    'smiles_node_featurizer': CanonicalAtomFeaturizer(),
    'smiles_edge_featurizer': CanonicalBondFeaturizer(),
    'load_checkpoint': True
}

v3 = {
    'random_seed': 8,
    'mlp_hidden_layer': 64,
    'gat_hidden_feats': [32],
    'fasta_node_feat_size': 15,
    'fasta_edge_feat_size': 1,
    'smiles_num_layers': 1,
    'smiles_num_timesteps': 1,
    'smiles_graph_feat_size': 200,
    'fasta_num_layers': 2,
    'fasta_num_timesteps': 2,
    'fasta_graph_feat_size': 200,
    'n_tasks': 1,
    'dropout': 0,
    'weight_decay': 10 ** (-5.0),
    'lr': 0.001,
    'batch_size': 128,
    'num_epochs': 1000,
    'frac_train': 0.8,
    'frac_val': 0.1,
    'frac_test': 0.1,
    'patience': 50,
    'metric_name': 'mae',
    'model': 'mhGANN',
    'mode': 'lower',
    'smiles_to_graph': smiles_to_bigraph,
    'smiles_node_featurizer': CanonicalAtomFeaturizer(),
    'smiles_edge_featurizer': CanonicalBondFeaturizer(),
    'load_checkpoint': False
}

v4 = {
    'random_seed': 8,
    'mlp_hidden_layer': 128,
    'gat_hidden_feats': [32, 32],
    'fasta_node_feat_size': 9,
    'fasta_edge_feat_size': 0,
    'smiles_num_layers': 2,
    'smiles_num_timesteps': 2,
    'smiles_graph_feat_size': 200,
    'fasta_num_layers': 2,
    'fasta_num_timesteps': 2,
    'fasta_graph_feat_size': 200,
    'n_tasks': 1,
    'dropout': 0,
    'weight_decay': 10 ** (-5.0),
    'lr': 0.00005,
    'batch_size': 128,
    'num_epochs': 1000,
    'frac_train': 0.8,
    'frac_val': 0.1,
    'frac_test': 0.1,
    'patience': 50,
    'metric_name': 'mae',
    'model': 'mhGANN',
    'mode': 'lower',
    'smiles_to_graph': partial(smiles_to_complete_graph, add_self_loop=True),
    'smiles_node_featurizer': WeaveAtomFeaturizer(),
    'smiles_edge_featurizer': WeaveEdgeFeaturizer(max_distance=2),
    'load_checkpoint': True
}

configs = {
    'v1': v1,
    'v2': v2,
    'v3': v3,
    'v4': v4
}

def get_config(config_name):
    return configs[config_name]