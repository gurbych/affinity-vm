# -*- coding: utf-8 -*-
#
# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0
#
# AttentiveFP
# pylint: disable= no-member, arguments-differ, invalid-name

import torch

import torch.nn as nn

from dgllife.model.gnn.attentivefp import AttentiveFPGNN
from dgllife.model.gnn.gat import GAT
from dgllife.model.readout import AttentiveFPReadout
from dgllife.model.model_zoo.mlp_predictor import MLPPredictor
from dgllife.model.readout.weighted_sum_and_max import WeightedSumAndMax

__all__ = ['mhGANN']

# pylint: disable=W0221
class mhGANN(nn.Module):
    """AttentiveFP for regression and classification on graphs.
    AttentiveFP is introduced in
    `Pushing the Boundaries of Molecular Representation for Drug Discovery with the Graph
    Attention Mechanism. <https://www.ncbi.nlm.nih.gov/pubmed/31408336>`__
    Parameters
    ----------
    node_feat_size : int
        Size for the input node features.
    edge_feat_size : int
        Size for the input edge features.
    num_layers : int
        Number of GNN layers. Default to 2.
    num_timesteps : int
        Times of updating the graph representations with GRU. Default to 2.
    graph_feat_size : int
        Size for the learned graph representations. Default to 200.
    n_tasks : int
        Number of tasks, which is also the output size. Default to 1.
    dropout : float
        Probability for performing the dropout. Default to 0.
    """
    def __init__(self,
                 smiles_node_feat_size,
                 smiles_edge_feat_size,
                 fasta_node_feat_size,
                 fasta_edge_feat_size,
                 smiles_num_layers=2,
                 smiles_num_timesteps=2,
                 smiles_graph_feat_size=200,
                 fasta_num_layers=2,
                 fasta_num_timesteps=2,
                 fasta_graph_feat_size=200,
                 n_tasks=1,
                 dropout=0.,
                 mlp_hidden_layer=None,
                 gat_hidden_feats=None):
        super(mhGANN, self).__init__()
        
#         print('*** AttentiveFPGNN ***')
#         print('smiles_node_feat_size:', smiles_node_feat_size)
#         print('smiles_edge_feat_size:', smiles_edge_feat_size)
#         print('smiles_num_layers:', smiles_num_layers)
#         print('smiles_graph_feat_size:', smiles_graph_feat_size)


        self.smiles_gnn = AttentiveFPGNN(node_feat_size=smiles_node_feat_size,
                                          edge_feat_size=smiles_edge_feat_size,
                                          num_layers=smiles_num_layers,
                                          graph_feat_size=smiles_graph_feat_size,
                                          dropout=dropout)
        
#         print(self.smiles_gnn)
        
#         self.fasta_gnn = AttentiveFPGNN(node_feat_size=fasta_node_feat_size,
#                                           edge_feat_size=fasta_edge_feat_size,
#                                           num_layers=fasta_num_layers,
#                                           graph_feat_size=fasta_graph_feat_size,
#                                           dropout=dropout)
        self.smiles_readout = AttentiveFPReadout(feat_size=smiles_graph_feat_size,
                                          num_timesteps=smiles_num_timesteps,
                                          dropout=dropout)
        self.fasta_gnn = GAT(in_feats=fasta_node_feat_size,
                             hidden_feats=gat_hidden_feats if gat_hidden_feats else [32, 32], # [64, 64] or [32, 32]
#                              feat_drops=[dropout, dropout],
#                              attn_drops=[dropout, dropout]
                            )
        
#         print('*** GAT ***')
#         print('fasta_node_feat_size:', fasta_node_feat_size)
#         print('gat_hidden_feats:', gat_hidden_feats)
#         print(self.fasta_gnn)
#         self.fasta_readout = AttentiveFPReadout(feat_size=fasta_graph_feat_size,
#                                           num_timesteps=fasta_num_timesteps,
#                                           dropout=dropout)
        if self.fasta_gnn.agg_modes[-1] == 'flatten':
            gnn_out_feats = self.fasta_gnn.hidden_feats[-1] * self.fasta_gnn.num_heads[-1]
        else:
            gnn_out_feats = self.fasta_gnn.hidden_feats[-1]
        self.fasta_readout = WeightedSumAndMax(gnn_out_feats)
        
        predictor_hidden_feats = mlp_hidden_layer if mlp_hidden_layer else 256 # 256 or 128
        self.predict = MLPPredictor(2 * gnn_out_feats + smiles_graph_feat_size,
                                    predictor_hidden_feats, n_tasks, dropout)
#         self.predict = nn.Sequential(
#             nn.Dropout(dropout),
#             nn.Linear(smiles_graph_feat_size+fasta_graph_feat_size, n_tasks)
#         )
        
#         print(self.predict)

    def forward(self, smiles_g, fasta_g,
                smiles_node_feats, smiles_edge_feats,
                fasta_node_feats, get_node_weight=False):
        """Graph-level regression/soft classification.
        Parameters
        ----------
        g : DGLGraph
            DGLGraph for a batch of graphs.
        node_feats : float32 tensor of shape (V, node_feat_size)
            Input node features. V for the number of nodes.
        edge_feats : float32 tensor of shape (E, edge_feat_size)
            Input edge features. E for the number of edges.
        get_node_weight : bool
            Whether to get the weights of atoms during readout. Default to False.
        Returns
        -------
        float32 tensor of shape (G, n_tasks)
            Prediction for the graphs in the batch. G for the number of graphs.
        node_weights : list of float32 tensor of shape (V, 1), optional
            This is returned when ``get_node_weight`` is ``True``.
            The list has a length ``num_timesteps`` and ``node_weights[i]``
            gives the node weights in the i-th update.
        """
        smiles_node_feats = self.smiles_gnn(smiles_g, smiles_node_feats, smiles_edge_feats)
        fasta_node_feats = self.fasta_gnn(fasta_g, fasta_node_feats)
        if get_node_weight:
            smiles_g_feats, smiles_node_weights = self.smiles_readout(smiles_g, smiles_node_feats, get_node_weight)
            fasta_g_feats = self.fasta_readout(fasta_g, fasta_node_feats)
            g_feats = torch.cat((smiles_g_feats, fasta_g_feats), dim=1)
            return self.predict(g_feats), smiles_node_weights, fasta_g_feats
        else:
            smiles_g_feats = self.smiles_readout(smiles_g, smiles_node_feats, get_node_weight)
            fasta_g_feats = self.fasta_readout(fasta_g, fasta_node_feats)
            g_feats = torch.cat((smiles_g_feats, fasta_g_feats), dim=1)
            return self.predict(g_feats)