import re
import numpy as np


def parse_descr_file(filename):
    dict = {}
    fh = open(filename, 'r')
    for line in fh:
        arr = re.split('\s+',line.strip())
        if len(arr) == 3:
            dict[arr[0]] = float(arr[2])
    vals = list(dict.values())
    mean = np.mean(vals)
    std = np.std(vals)
    for key in dict:
        dict[key] = (dict[key] - mean)/std
    return dict


def collect_feat_dicts(prefix):
    feat_dicts = []
#     feat_dicts.append(parse_descr_file(prefix + 'AliphaticIndex_Ikai'))
#     feat_dicts.append(parse_descr_file(prefix + 'AvrAreaBuried_Rose'))
    feat_dicts.append(parse_descr_file(prefix + 'Charge'))
#     feat_dicts.append(parse_descr_file(prefix + 'DiSulfides'))
    feat_dicts.append(parse_descr_file(prefix + 'Flexibility_Smith'))
#     feat_dicts.append(parse_descr_file(prefix + 'FoldedUnfolded_Garbuzynskiy'))
    feat_dicts.append(parse_descr_file(prefix + 'HB_acceptors'))
    feat_dicts.append(parse_descr_file(prefix + 'HB_donors'))
    feat_dicts.append(parse_descr_file(prefix + 'Hydrophobicity_Eisenberg'))
    feat_dicts.append(parse_descr_file(prefix + 'MaxASA_Tien'))
    feat_dicts.append(parse_descr_file(prefix + 'MW'))
#     feat_dicts.append(parse_descr_file(prefix + 'pI'))
    feat_dicts.append(parse_descr_file(prefix + 'Polarity_Zimmerman'))
#     feat_dicts.append(parse_descr_file(prefix + 'Refractivity_Jones'))
    feat_dicts.append(parse_descr_file(prefix + 'VDW_Volume'))
    return feat_dicts