import dgl.backend as F
import numpy as np
import pandas as pd
import os
import torch
import random
import dgl
import pickle
import os
# import traceback

from dgllife.utils import smiles_to_bigraph
from dgl.data.utils import save_graphs, load_graphs
from .fasta_feat_dicts import collect_feat_dicts
from dgllife.utils import Meter
from rdkit import Chem
from dgl import DGLGraph

INF = False


class AffinityDataset(object):
    def __init__(self,
                 smiles_to_graph=smiles_to_bigraph,
                 smiles_node_featurizer=None,
                 smiles_edge_featurizer=None,
                 fasta_col_name="FASTA",
                 smiles_col_name="SMILES",
                 target_col_name="Ki (nM)",
                 data_path = '../data/fasta-smiles-ki/fasta-smiles-ki-master.csv',
                 log_every=1000,
                 cache_size=10000,
                 load=True,
                 inference=False):
        # for FASTA descriptors
        self.prefix = 'affinity_module/FASTA_descriptors/'
        self.feat_dicts = collect_feat_dicts(self.prefix)
        # graph creation
        self.df = pd.read_csv(data_path)
#         self.df = pd.read_csv(data_path)[:10000]
        self.smiles = []
        self.fasta = []
        self.smiles_graphs = []
        self.fasta_graphs = []
        self.labels = []
        self.smiles_graphs_cache_path = "graph_cache_2/smiles_graphs/smiles_dglgraph"
        self.fasta_graphs_cache_path = "graph_cache_2/fasta_graphs/fasta_dglgraph"
        self.smiles_cache_path = "graph_cache_2/smiles/smiles"
        self.fasta_cache_path = "graph_cache_2/fasta/fasta"
        self.labels_cache_path = "graph_cache_2/labels/label"
#         self.smiles_graphs_cache_path = "graph_cache_inference/smiles_graphs/smiles_dglgraph"
#         self.fasta_graphs_cache_path = "graph_cache_inference/fasta_graphs/fasta_dglgraph"
#         self.smiles_cache_path = "graph_cache_inference/smiles/smiles"
#         self.fasta_cache_path = "graph_cache_inference/fasta/fasta"
#         self.labels_cache_path = "graph_cache_inference/labels/label"
        self.cache_size = cache_size
        self.inference = inference

        self._makedir(self.smiles_graphs_cache_path)
        self._makedir(self.fasta_graphs_cache_path)
        self._makedir(self.smiles_cache_path)
        self._makedir(self.fasta_cache_path)
        self._makedir(self.labels_cache_path)
        
        if target_col_name is None:
            self.task_names = self.df.columns.drop([smiles_col_name, fasta_col_name]).tolist()
        else:
            self.task_names = [target_col_name]
        
        self.n_tasks = len(self.task_names)
        self._pre_process(smiles_to_graph, smiles_node_featurizer, smiles_edge_featurizer,
                          smiles_col_name, fasta_col_name, load, log_every)
    
    def _makedir(self, path):
        dirname = os.path.dirname(path) 
        if not os.path.exists(dirname):
            os.makedirs(dirname)
            
    def get_aa_list(self):
        return ['A', 
                'R',
                'N',
                'D',
                'C',
                'Q',
                'E',
                'G',
                'H',
                'I',
                'L',
                'K',
                'M',
                'F',
                'P',
                'S',
                'T',
                'W',
                'Y',
                'V']
        
    def _make_node_features(self, fasta):
#         aa_list = self.get_aa_list()
#         aa_dict = {v: i for i,v in enumerate(aa_list)} 
#         features = torch.zeros([len(fasta), len(self.feat_dicts) + len(aa_list)], dtype=torch.float32)
        features = torch.zeros([len(fasta), len(self.feat_dicts)], dtype=torch.float32)
        for aai in range(len(fasta)):
            for fi in range(len(self.feat_dicts)):
                features[aai][fi] = self.feat_dicts[fi][fasta[aai]]
#             aa_index = aa_dict.get(fasta[aai])
#             features[aai][len(self.feat_dicts)+aa_index]
        return features
    
#     def _make_edge_features(self, fasta):
#         features = torch.zeros([len(fasta)-1, 1], dtype=torch.float32)
#         for aai in range(len(fasta)-1):
#             features[aai] = 1
#         return features


    def _fasta_to_graph(self, fasta):
        """
        g (N, )
        f (N, F)
        """
        edges = list()
        for i in range(len(fasta)-1):
            edges.append((i, i+1))
        g = DGLGraph(edges)
        node_features = self._make_node_features(fasta)
#         edge_features = self._make_edge_features(fasta)
        g.ndata.update({'h': node_features})
#         g.edata.update({'e': edge_features})
        return g

    
    def _pre_process(self, smiles_to_graph, node_featurizer, edge_featurizer,
                     smiles_col_name, fasta_col_name, load, log_every):
        """Pre-process the dataset
        * Convert molecules from smiles format into DGLGraphs
          and featurize their atoms
        Parameters
        ----------
        smiles_to_graph : callable, SMILES -> DGLGraph
            Function for converting a SMILES (str) into a DGLGraph.
        node_featurizer : callable, rdkit.Chem.rdchem.Mol -> dict
            Featurization for nodes like atoms in a molecule, which can be used to update
            ndata for a DGLGraph.
        edge_featurizer : callable, rdkit.Chem.rdchem.Mol -> dict
            Featurization for edges like bonds in a molecule, which can be used to update
            edata for a DGLGraph.
        load : bool
            Whether to load the previously pre-processed dataset or pre-process from scratch.
            ``load`` should be False when we want to try different graph construction and
            featurization methods and need to preprocess from scratch. Default to True.
        log_every : bool
            Print a message every time ``log_every`` molecules are processed.
        """
        if load:
            bad_idx_glob = []
            # DGLGraphs have been constructed before, reload them
            print('Loading previously saved dgl graphs...')
            smiles_graphs_cache_dir = os.path.dirname(self.smiles_graphs_cache_path)
            fasta_graphs_cache_dir = os.path.dirname(self.fasta_graphs_cache_path)
            smiles_cache_dir = os.path.dirname(self.smiles_cache_path)
            fasta_cache_dir = os.path.dirname(self.fasta_cache_path)
            labels_dir = os.path.dirname(self.labels_cache_path)
            
            smiles_graphs_cache_list = sorted([f for f in os.listdir(smiles_graphs_cache_dir) if f.endswith('.bin')])
            fasta_graphs_cache_list = sorted([f for f in os.listdir(fasta_graphs_cache_dir) if f.endswith('.bin')])
            smiles_cache_list = sorted([f for f in os.listdir(smiles_cache_dir) if f.endswith('.bin')])
            fasta_cache_list = sorted([f for f in os.listdir(fasta_cache_dir) if f.endswith('.bin')])
            if not self.inference:
                labels_list = sorted([f for f in os.listdir(labels_dir) if f.endswith('.bin')])
            
#             for smiles_graphs_cache_file,\
#                 fasta_graphs_cache_file,\
#                 smiles_cache_file,\
#                 fasta_cache_file,\
#                 labels_cache_file in zip(smiles_graphs_cache_list,
#                                          fasta_graphs_cache_list,
#                                          smiles_cache_list,
#                                          fasta_cache_list,
#                                          labels_list):
            if self.inference:
                for smiles_graphs_cache_file,\
                    fasta_graphs_cache_file,\
                    smiles_cache_file,\
                    fasta_cache_file in zip(smiles_graphs_cache_list,
                                             fasta_graphs_cache_list,
                                             smiles_cache_list,
                                             fasta_cache_list):
                    smiles_graphs_cache_file = os.path.join(smiles_graphs_cache_dir, smiles_graphs_cache_file)
                    fasta_graphs_cache_file = os.path.join(fasta_graphs_cache_dir, fasta_graphs_cache_file)
                    smiles_cache_file = os.path.join(smiles_cache_dir, smiles_cache_file)
                    fasta_cache_file = os.path.join(fasta_cache_dir, fasta_cache_file)

                    smiles_graphs, _ = load_graphs(smiles_graphs_cache_file)
                    fasta_graphs, _ = load_graphs(fasta_graphs_cache_file)

                    self.smiles_graphs.extend(smiles_graphs)
                    self.fasta_graphs.extend(fasta_graphs)

                    with open(smiles_cache_file, 'rb') as f:
                        self.smiles.extend(pickle.load(f))
                    with open(fasta_cache_file, 'rb') as f:
                        self.fasta.extend(pickle.load(f))
            else:
                for smiles_graphs_cache_file,\
                    fasta_graphs_cache_file,\
                    smiles_cache_file,\
                    fasta_cache_file,\
                    labels_cache_file in zip(smiles_graphs_cache_list, # [:32] to limit cache blocks
                                             fasta_graphs_cache_list, # [:32]
                                             smiles_cache_list, # [:32]
                                             fasta_cache_list, # [:32]
                                             labels_list): # [:32]
                    smiles_graphs_cache_file = os.path.join(smiles_graphs_cache_dir, smiles_graphs_cache_file)
                    fasta_graphs_cache_file = os.path.join(fasta_graphs_cache_dir, fasta_graphs_cache_file)
                    smiles_cache_file = os.path.join(smiles_cache_dir, smiles_cache_file)
                    fasta_cache_file = os.path.join(fasta_cache_dir, fasta_cache_file)
                    labels_cache_file = os.path.join(labels_dir, labels_cache_file)

                    smiles_graphs, _ = load_graphs(smiles_graphs_cache_file)
                    fasta_graphs, _ = load_graphs(fasta_graphs_cache_file)

                    self.smiles_graphs.extend(smiles_graphs)
                    self.fasta_graphs.extend(fasta_graphs)

                    with open(smiles_cache_file, 'rb') as f:
                        self.smiles.extend(pickle.load(f))
                    with open(fasta_cache_file, 'rb') as f:
                        self.fasta.extend(pickle.load(f))
                    with open(labels_cache_file, 'rb') as f:
                        self.labels.extend(pickle.load(f))
                
            
            print('smiles:',        len(self.smiles))
            print('fasta:',         len(self.fasta))
            print('smiles_graphs:', len(self.smiles_graphs))
            print('fasta_graphs:',  len(self.fasta_graphs))
            if not self.inference:
                self.labels = F.zerocopy_from_numpy(np.nan_to_num(self.labels).astype(np.float32))
                print('labels:',        len(self.labels))
        else:
            j_offset = 0
#             j_offset = 330000
            i_offset = 1
            print('Processing dgl graphs from scratch...')
            smiles_raw = self.df[smiles_col_name].values.tolist()
            fasta_raw = self.df[fasta_col_name].values.tolist()
            labels_raw = self.df[self.task_names].values.tolist()
            last = len(smiles_raw) - 1
            prev = 0
            j = 0
            for i, (s, f, l) in enumerate(zip(smiles_raw, fasta_raw, labels_raw)):
                if (i + i_offset) % log_every == 0:
                    print('Processing graph {:d}/{:d}'.format(i+i_offset, len(self)))

                try:
                    sg = smiles_to_graph(s, node_featurizer=node_featurizer, edge_featurizer=edge_featurizer)
                    fg = self._fasta_to_graph(f.upper())
                
                    self.smiles_graphs.append(sg)
                    self.fasta_graphs.append(fg)
                    self.smiles.append(s)
                    self.fasta.append(f)
                    if not self.inference:
                        self.labels.append(l)
                    j += 1
                except Exception as e:
                    print(e)
#                     traceback.print_exc()
                    
                if (j + 1) % self.cache_size == 0 or i == last:
                    print('Caching graph {:d}/{:d}'.format(j+j_offset, len(self)))
                    save_graphs('{}_{:07d}.bin'.format(self.smiles_graphs_cache_path, j+j_offset+1), self.smiles_graphs[prev:j])
                    save_graphs('{}_{:07d}.bin'.format(self.fasta_graphs_cache_path, j+j_offset+1), self.fasta_graphs[prev:j])
                        
                    with open('{}_{:07d}.bin'.format(self.smiles_cache_path, j+j_offset+1), 'wb') as f:
                        pickle.dump(self.smiles[prev:j], f)
                    with open('{}_{:07d}.bin'.format(self.fasta_cache_path, j+j_offset+1), 'wb') as f:
                        pickle.dump(self.fasta[prev:j], f)
                    if not self.inference:
                        with open('{}_{:07d}.bin'.format(self.labels_cache_path, j+j_offset+1), 'wb') as f:
                            pickle.dump(self.labels[prev:j], f)
                    prev = j
                    
            print('smiles:', len(self.smiles))
            print('fasta:',len(self.fasta))
            print('smiles_graphs:',len(self.smiles_graphs))
            print('fasta_graphs:',len(self.fasta_graphs))
            if not self.inference:
                print('labels:',len(self.labels))
                # np.nan_to_num will also turn inf into a very large number
                self.labels = F.zerocopy_from_numpy(np.nan_to_num(self.labels).astype(np.float32))
    
    def __getitem__(self, item):
        """Get datapoint with index
        Parameters
        ----------
        item : int
            Datapoint index
        Returns
        -------
        str
            SMILES for the ith datapoint
        DGLGraph
            DGLGraph for the ith datapoint
        Tensor of dtype float32 and shape (T)
            Labels of the datapoint for all tasks
        """
        if self.inference:
            return self.fasta[item], self.fasta_graphs[item], self.smiles[item], self.smiles_graphs[item]
        else:
            return self.fasta[item], self.fasta_graphs[item], self.smiles[item], self.smiles_graphs[item], self.labels[item]

    def __len__(self):
        """Size for the dataset
        Returns
        -------
        int
            Size for the dataset
        """
        return len(self.smiles)

    
def regress(args, model, bg, fbg, get_weights=False):
    atom_feats, bond_feats = bg.ndata.pop('h'), bg.edata.pop('e')
#     atom_feats, bond_feats = bg.ndata.pop(), bg.edata.pop()
    atom_feats, bond_feats = atom_feats.to(args['device']), bond_feats.to(args['device'])
#     aa_feats, aa_bond_feats = fbg.ndata.pop('h'), fbg.edata.pop('e')
    aa_feats = fbg.ndata.pop('h')
    aa_feats = aa_feats.to(args['device'])
#     return model(bg, fbg, atom_feats, bond_feats, aa_feats, aa_bond_feats, get_weights)
    return model(bg, fbg, atom_feats, bond_feats, aa_feats, get_weights)

def run_a_train_epoch(args, epoch, model, data_loader,
                      loss_criterion, optimizer):
    model.train()
    train_meter = Meter()
    for batch_id, batch_data in enumerate(data_loader):
        fasta, fbg, smiles, bg, labels = batch_data
        labels = labels.to(args['device'])
#         print('SMILES input graph:', bg, 'FASTA input graph:', fbg)
        prediction = regress(args, model, bg, fbg)
        loss = loss_criterion(prediction, labels).mean()
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        train_meter.update(prediction, labels)
    total_score = np.mean(train_meter.compute_metric(args['metric_name']))
    print('epoch {:d}/{:d}, training {} {:.4f}'.format(
        epoch + 1, args['num_epochs'], args['metric_name'], total_score))

def run_an_eval_epoch(args, model, data_loader):
    model.eval()
    eval_meter = Meter()
    with torch.no_grad():
        for batch_id, batch_data in enumerate(data_loader):
            fasta, fbg, smiles, bg, labels = batch_data
            labels = labels.to(args['device'])
            prediction = regress(args, model, bg, fbg)
            eval_meter.update(prediction, labels)
        total_score = np.mean(eval_meter.compute_metric(args['metric_name']))
    return total_score

def run_an_inference_epoch(args, model, data_loader):
    model.eval()
    predictions_by_batch = []
    fastas = []
    smiless = []
    with torch.no_grad():
        for batch_id, batch_data in enumerate(data_loader):
            fasta, fbg, smiles, bg = batch_data
            prediction = regress(args, model, bg, fbg)
            predictions_by_batch.append(prediction)
            fastas.append(fasta)
            smiless.append(smiles)
    return fastas, smiless, predictions_by_batch

def run_visualization_epoch(args, model, data_loader, index):
    model.eval()
    eval_meter = Meter()
    with torch.no_grad():
        for batch_id, batch_data in enumerate(data_loader):
            if batch_id == index:
                fasta, fbg, smiles, bg, labels = batch_data
                labels = labels.to(args['device'])
                prediction, smiles_node_weights, fasta_node_weights = regress(args, model, bg, fbg, get_weights=True)
                eval_meter.update(prediction, labels)
                score = eval_meter.compute_metric(args['metric_name'])
                return smiles, smiles_node_weights, fasta, fasta_node_weights, labels, score

def set_random_seed(seed=0):
    """Set random seed.
    Parameters
    ----------
    seed : int
        Random seed to use. Default to 0.
    """
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)
        
def collate_molgraphs(data):
    """Batching a list of datapoints for dataloader.
    Parameters
    ----------
    data : list of 4-tuples.
        Each tuple is for a single datapoint, consisting of
        a SMILES, a DGLGraph, all-task labels
    Returns
    -------
    smiles : list
        List of smiles
    bg : DGLGraph
        The batched DGLGraph.
    labels : Tensor of dtype float32 and shape (B, T)
        Batched datapoint labels. B is len(data) and
        T is the number of total tasks.
    """
    if INF:
        fasta, fasta_graphs, smiles, graphs = map(list, zip(*data))
    else:
        fasta, fasta_graphs, smiles, graphs, labels = map(list, zip(*data))

    bg = dgl.batch(graphs)
    bg.set_n_initializer(dgl.init.zero_initializer)
    bg.set_e_initializer(dgl.init.zero_initializer)
    fbg = dgl.batch(fasta_graphs)
    fbg.set_n_initializer(dgl.init.zero_initializer)
    fbg.set_e_initializer(dgl.init.zero_initializer)
    
    if not INF:
        labels = torch.stack(labels, dim=0)

    if INF:
        return fasta, fbg, smiles, bg
    else:
        return fasta, fbg, smiles, bg, labels

def load_model(args):
    if args['model'] == 'AttentiveFP':
        from dgllife.model import AttentiveFPPredictor
        model = AttentiveFPPredictor(node_feat_size=args['node_featurizer'].feat_size('h'),
                                     edge_feat_size=args['edge_featurizer'].feat_size('e'),
                                     num_layers=args['num_layers'],
                                     num_timesteps=args['num_timesteps'],
                                     graph_feat_size=args['graph_feat_size'],
                                     n_tasks=args['n_tasks'],
                                     dropout=args['dropout'])
    elif args['model'] == 'mhGANN':
        from affinity_module.model import mhGANN
        model = mhGANN(
                       smiles_node_feat_size=args['smiles_node_featurizer'].feat_size(),       # WeaveAtomFeaturizer
#                        smiles_node_feat_size=args['smiles_node_featurizer'].feat_size('h'),  # CanonicalAtomFeaturizer
                       smiles_edge_feat_size=args['smiles_edge_featurizer'].feat_size(),       # WeaveEdgeFeaturizer
#                        smiles_edge_feat_size=args['smiles_edge_featurizer'].feat_size('e'),  # CanonicalAtomFeaturizer
                       fasta_node_feat_size=args['fasta_node_feat_size'],
                       fasta_edge_feat_size=args['fasta_edge_feat_size'],
                       smiles_num_layers=args['smiles_num_layers'],
                       smiles_num_timesteps=args['smiles_num_timesteps'],
                       smiles_graph_feat_size=args['smiles_graph_feat_size'],
                       fasta_num_layers=args['fasta_num_layers'],
                       fasta_num_timesteps=args['fasta_num_timesteps'],
                       fasta_graph_feat_size=args['fasta_graph_feat_size'],
                       n_tasks=args['n_tasks'],
                       dropout=args['dropout'],
                       mlp_hidden_layer=args['mlp_hidden_layer'],
                       gat_hidden_feats=args['gat_hidden_feats'])

    return model